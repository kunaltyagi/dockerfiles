#!/usr/bin/env python

# @Author: Kunal Tyagi

import os
import struct
import sys

from array import array
from os import path

import png

# Original source: http://abel.ee.ucla.edu/cvxopt/_downloads/mnist.py
# Modified source: https://github.com/myleott/mnist_png
def read(dataset, files):
    fname_img = next((f for f in files if 'images' in f))
    fname_lbl = next((f for f in files if 'labels' in f))

    flbl = open(fname_lbl, 'rb')
    magic_nr, size = struct.unpack(">II", flbl.read(8))
    lbl = array("b", flbl.read())
    flbl.close()

    fimg = open(fname_img, 'rb')
    magic_nr, size, rows, cols = struct.unpack(">IIII", fimg.read(16))
    img = array("B", fimg.read())
    fimg.close()

    return lbl, img, size, rows, cols

def write_dataset(labels, data, size, rows, cols, output_dir, sub_dirs=10):
    # create output directories
    output_dirs = [
        path.join(output_dir, str(i))
        for i in range(sub_dirs)
    ]
    for dir in output_dirs:
        if not path.exists(dir):
            os.makedirs(dir)
    # write data
    for (i, label) in enumerate(labels):
        output_filename = path.join(output_dirs[label], str(i) + ".png")
        print("writing " + output_filename)
        with open(output_filename, "wb") as h:
            w = png.Writer(cols, rows, greyscale=True)
            data_i = [
                data[ (i*rows*cols + j*cols) : (i*rows*cols + (j+1)*cols) ]
                for j in range(rows)
            ]
            w.write(h, data_i)

if __name__ == "__main__":
    if len(sys.argv) not in [3, 4]:
        print("usage: {0} <input_path> <output_path> <optional_subset_for_emnist>".format(sys.argv[0]))
        sys.exit()

    input_path = os.path.abspath(sys.argv[1])
    output_path = os.path.abspath(sys.argv[2])
    options = 'byclass' if len(sys.argv) == 3 else sys.argv[3]

    input_files = list()
    for f in os.listdir(input_path):
        f_abs = os.path.join(input_path, f)
        if os.path.isfile(f_abs) and f.split('-')[-1] == 'ubyte':
            input_files.append(f)
    
    is_emnist = any(['emnist' in f for f in input_files])
    count = 10
    if is_emnist:
        input_files = [f for f in input_files if 'emnist' in f and options in f]
        mapping_file = '-'.join(input_files[0].split('-')[:2]) + '-mapping.txt'
        with open(os.path.join(input_path, mapping_file)) as f:
            count = sun(1 for line in f)
        
    if not len(input_files):
        raise ValueError("Can't find the ubyte files. Did you gunzip them?")
   
    dataset = dict() 
    dataset["training"] = [f for f in input_files if "train" in f]
    dataset["testing"]  = [f for f in input_files if "train" not in f]
    if not len(dataset["training"]) == len(dataset["testing"]):
        raise ValueError("Expected equal number of training and testing files")

    for key, value in dataset.items():
        files = [os.path.join(input_path, f) for f in value]
        labels, data, size, rows, cols = read(key, files)
        write_dataset(labels, data, size, rows, cols,
                      path.join(output_path, key), count)
