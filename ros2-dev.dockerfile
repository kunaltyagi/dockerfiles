FROM osrf/ros2:ardent-basic

MAINTAINER Kunal Tyagi

RUN apt update && \
    apt install -y \
                build-essential clang cmake cppcheck git wget \
                libboost-all-dev libeigen3-dev libopencv-dev libtinyxml-dev \
                libasio-dev libtinyxml2-dev \
                python-empy \
                python3-catkin-pkg-modules python3-dev \
                python3-empy python3-nose python3-pip python3-pyparsing \
                python3-setuptools python3-vcstool python3-yaml \
                ros-ardent-ament* && \
    apt install -y \
                clang-format pydocstyle pyflakes \
                python3-coverage python3-mock python3-pep8 \
                uncrustify && \
    pip3 install --upgrade \
                 argcomplete flake8 flake8-blind-except flake8-builtins \
                 flake8-class-newline flake8-comprehensions flake8-deprecated \
                 flake8-docstrings flake8-import-order flake8-quotes \
                 pytest pytest-cov pytest-runner
