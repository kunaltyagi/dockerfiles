FROM kunaltyagi/cpp-dev

MAINTAINER Kunal Tyagi

RUN apt install -y libxml2-dev libcppunit-dev libssh2-1-dev libc-ares-dev \
                   zlib1g-dev libsqlite3-dev pkg-config libssl-dev && \
    rm -rf /var/lib/apt/lists/* && \
    cd / && \
    git clone https://github.com/aria2/aria2 --depth=1

RUN cd /aria2 && \
    git pull origin && \
    autoreconf -i; autoreconf -i && \
    ./configure && \
    make -j8 && \
    ln -s /aria2/src/aria2c /usr/bin/aria2c
