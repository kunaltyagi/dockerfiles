FROM kunaltyagi/cpp-dev

MAINTAINER Kunal Tyagi

RUN apt install -y \
                python3 \
                python3-venv && \
    python3 -m venv /python

RUN . /python/bin/activate && \
    pip install -U pip \
                   scipy \
                   opencv-python
