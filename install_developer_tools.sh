#! /usr/bin/env sh

function __prepare_input__() {
# ensure all actions are repeatable without causing harm
# such as: write to empty files only
group=
req=
install=
update=
post=
[ $1 == '-g' ] && group=true
[ $1 == '-r' ] && req=true
[ $1 == '-p' ] && post=true
[ $1 == '-u' ] && update=true
if [ $1 == '-i' ]; then
    req=true
    update=true
    install=true
    post=true
fi

}

function basic_tools() {
# the basics required to setup for installation
__prepare_input__ $1

local pkgs='apt-transport-https ca-certificates curl lsb-release
software-properties-common wget'
[ $group ] && echo ${pkgs}
[ $install ] && apt update && apt install -y ${pkgs}
}

function basic_tools_gui() {
__prepare_input__ $1
local pkgs='guake tilix xsettingsd'

[ $group ] && echo ${pkgs}
[ $req ] && add-apt-repository ppa:webupd8team/terminix
[ $update ] && apt update
[ $install ] && apt install -y ${pkgs}
[ $post ] && update-alternatives --config x-terminal-emulator
}

function browsing_tools_gui() {
__prepare_input__ $1
local pkgs='firefox chromium-browser chrome-gnome-shell'

[ $group ] && echo ${pkgs}
[ $update ] && apt update
[ $install ] && apt install -y ${pkgs}
}

function cli_tools() {
__prepare_input__ $1

local pkgs='stow tree'
[ $group ] && echo ${pkgs}
[ $install ] && apt install -y ${pkgs}
}

function container_tools_prep() {
local os=$(lsb_release -d | awk '{print $2}' | tr A-Z a-z)
local arch=$(dpkg --print-architecture)

curl -fsSL https://download.docker.com/linux/${os}/gpg | apt-key add -
sources_file=/etc/apt/sources.list.d/docker.list
echo "deb [arch=${arch}] https://download.docker.com/linux/${os} $(lsb_release -cs) edge
# deb-src [arch=${arch}] https://download.docker.com/linux/${os} $(lsb_release -cs) edge" > ${sources_file}
}
function container_tools() {
__prepare_input__ $1
local pkgs='docker-ce qemu-kvm'

[ $group ] && echo ${pkgs}
[ $req ] && container_tools_prep
[ $update ] && apt update
[ $install ] && apt install -y ${pkgs}
if [ $post ]; then
    usermod -a -G docker $USER
    adduser $USER kvm
fi
}

function cross_compile_tools_prep() {
# ensure that dpkg doesn't go mad if other architectures are added
local arch=$(dpkg --print-architecture)
local main_file=/etc/apt/sources.list
for file in ${main_file} `ls ${main_file}.d/*.list 2> /dev/null`
do
    sed -i "s/deb\(-src\)\? h/deb\1 [arch=${arch}] h/" $file
done

local sources_file=/etc/apt/sources.list.d/embedded.list
local os=$(lsb_release -d | awk '{print $2}' | tr A-Z a-z)

# TODO: use case instead
if [ $os == 'ubuntu' ]; then
    echo -n "" > ${sources_file}
    for suffix in '' -updates -security
    do
        echo "deb [arch=armhf] http://ports.ubuntu.com/ubuntu-ports $(lsb_release -cs)${suffix} main restricted universe multiverse" >> ${sources_file}
    done
elif [ $os == 'debian' ]; then
    curl http://emdebian.org/tools/debian/emdebian-toolchain-archive.key | apt-key add -
    echo "deb http://emdebian.org/tools/debian $(lsb_release -cs) main" > ${sources_file}
fi

dpkg --add-architecture armhf
}
function cross_compile_tools() {
__prepare_input__ $1
local pkgs='binutils-arm-linux-gnueabi crossbuild-essential-armhf'

[ $group ] && echo ${pkgs}
[ $req ] && cross_compile_tools_prep
[ $update ] && apt update
[ $install ] && apt install -y ${pkgs}
[ $post ] && sudo ln -s /usr/arm-linux-gnueabihf/lib/ld-linux-armhf.so.3 /lib/
}
function qemu_tools() {
__prepare_input__ $1
local pkgs='binutils debootstrap live-build qemu qemu-user-static'

[ $group ] && echo ${pkgs}
[ $update ] && apt update
[ $install ] && apt install -y ${pkgs}
}

function debugging_tools() {
__prepare_input__ $1
local pkgs='gdb strace trace-cmd valgrind'

[ $group ] && echo ${pkgs}
[ $install ] && apt install -y ${pkgs}
}

function debugging_tools_gui() {
__prepare_input__ $1
local pkgs='valkyrie kernelshark'

[ $group ] && echo ${pkgs}
[ $update ] && apt update
[ $install ] && apt install -y ${pkgs}
}

function dev_tools_prep() {
local ppa_list=(ubuntu-toolchain-r/test git-core/ppa)
for ppa in "${ppa_list[@]}"
do
    if ! `grep -q "^deb .*${ppa}" /etc/apt/sources.list /etc/apt/sources.list.d/*.list`; then
        add-apt-repository -y ppa:${ppa};
    fi
done

wget -O - https://apt.llvm.org/llvm-snapshot.gpg.key | apt-key add -
local sources_file=/etc/apt/sources.list.d/llvm.list
local arch=$(dpkg --print-architecture)
echo "deb [arch=${arch}] http://apt.llvm.org/$(lsb_release -cs)/ llvm-toolchain-$(lsb_release -cs) main
# deb-src [arch=${arch}] http://apt.llvm.org/$(lsb_release -cs)/ llvm-toolchain-$(lsb_release -cs) main" > ${sources_file}
}
function dev_tools() {
__prepare_input__ $1
local pkgs='autoconf autoconf-archive automake autopoint autotools-dev build-essential
ccache cmake libtool ninja-build pkg-config clang gcc g++ llvm clang-tidy
git mc mercurial p7zip-full xclip'

[ $group ] && echo ${pkgs}
[ $req ] && dev_tools_prep
[ $update ] && apt update
[ $install ] && apt install -y ${pkgs}
if [ $post ]; then
    update-alternatives --config cc
    update-alternatives --config c++
fi
}

function editing_tools() {
__prepare_input__ $1
local pkgs='neovim emacs-nox' # python3-neovim

[ $group ] && echo ${pkgs}
[ $req ] && apt-add-repository ppa:neovim-ppa/stable
[ $update ] && apt update
[ $install ] && apt install -y ${pkgs}
}

function goodies() {
    # apt-utils
    # debian-goodies
echo Hi
}

function image_tools() {
    echo ho
}

function monitoring_tools() {
__prepare_input__ $1
local pkgs='htop iftop iotop nmap progress pv tshark'

[ $group ] && echo ${pkgs}
[ $update ] && apt update
[ $install ] && apt install -y ${pkgs}
}

function monitoring_tools_gui() {
__prepare_input__ $1
local pkgs='wireshark'

[ $group ] && echo ${pkgs}
[ $update ] && apt update
[ $install ] && apt install -y ${pkgs}
if [ $post ]; then
    groupadd -g wireshark
    usermod -a -G wireshark $USER
    dumpcap=`which dumpcap`
    chgrp wireshark $dumpcap
    chmod 4750 $dumpcap
fi
}

function python_tools() {
__prepare_input__ $1
local pkgs='python python3 python-dev python3-dev python3-pip python3-venv'

[ $group ] && echo ${pkgs}
[ $update ] && apt update
[ $install ] && apt install -y ${pkgs}
}

function remote_tools() {
__prepare_input__ $1
local pkgs='tmux openssh-server'

[ $group ] && echo ${pkgs}
[ $update ] && apt update
[ $install ] && apt install -y ${pkgs}
}

function video_tools() {
    # ffmpeg
    # vlc-noox
    # vlc ??
    # snap vlc??
echo Hi
}

function install_bare() {
    for i in "$@"
    do
        echo "Setting up $i"
        $i -r
    done

    apt update
    apt upgrade -y

    local pkgs=$(
    for i in "$@"
    do
        $i -g
    done
    )
    apt install $pkgs

    for i in "$@"
    do
        echo "Finishing installation of $i"
        $i -p
    done
}

function install_gui_check() {
    local gui=`echo $@ | grep -o '[[:alnum:]]\+_gui'`
    install_bare basic_tools basic_tools_gui
    if ! xset q &> /dev/null; then
        echo "# No X-server available. Will not install GUI tools" >&2
        local install=`echo ${gui} $@ | sort | uniq -u`
    else
        local install=$@
    fi
    install_bare $install
}

function install_dev() {
    install_bare basic_tools
    install_bare dev_tools debugging_tools editing_tools python_tools remote_tools
}

function install_env() {
    install_bare basic_tools
    install_bare cli_tools container_tools cross_compile_tools debugging_tools \
                 dev_tools editing_tools monitoring_tools python_tools remote_tools
}

function install_all() {
    install_gui_check cli_tools cross_compile_tools container_tools \
                 cross_compile_tools \
                 debugging_tools debugging_tools_gui dev_tools editing_tools \
                 monitoring_tools monitoring_tools_gui python_tools qemu_tools remote_tools
}

function help_install() {
    echo "install_[dev, env, all]"
}
