FROM kunaltyagi/cpp_dev

MAINTAINER Kunal Tyagi

RUN apt update && \
    apt install -y curl lsb-release build-essentials && \
    sed -i 's/deb\(-src\)\? h/deb\1 [arch=arm64] h/' /etc/apt/sources.list && \
    for suffix in '' -updates -security \
    do \
      echo "deb [arch=armhf] http://ports.ubuntu.com/ubuntu-ports $(lsb_release -cs)${suffix} main restricted unniverse multiverse" >> /etc/apt/embedded.list \
    done && \
    dpkg --add-architecture armhf && \
    apt update && \
    apt install -y crossbuild-essential-armhf
