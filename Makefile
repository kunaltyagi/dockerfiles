IMAGES = cpp-dev python-dev aria2 doxygen mnist ros2-dev sqlpp11-pg

.PHONY: ${IMAGES} all alpine install

all: ${IMAGES} Makefile
	echo "Done"

aria2 python-dev: cpp-dev

cpp-dev doxygen ros2-dev sqlpp11-pg:
	docker build --pull -t kunaltyagi/$@ - < $@.dockerfile

# Don't pull older image from docker
aria2 python-dev:
	docker build -t kunaltyagi/$@ - < $@.dockerfile

mnist:
	docker build --pull -t kunaltyagi/$@ . -f $@.dockerfile

install: all
	docker image ls | grep kunaltyagi | awk '{print $$1}' | xargs -n 1 -P 8 docker push

alpine: all
	docker image ls | grep kunaltyagi | awk '{print $$1}' | xargs -n 1 docker push
