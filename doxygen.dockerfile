FROM alpine

LABEL maintainer="Kunal Tyagi <tyagi.kunal@live.com>" \
      description="Doxygen, GraphViz and Git"

RUN apk --update add doxygen git graphviz && \
    rm -fr /var/cache/apk/*

CMD ["doxygen", "-v"]
