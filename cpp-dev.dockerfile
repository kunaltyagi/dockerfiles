FROM ubuntu:18.04

MAINTAINER Kunal Tyagi

RUN apt update && \
    apt upgrade -y && \
    apt install -y \
                autoconf autoconf-archive automake autopoint autotools-dev build-essential \
                ccache cmake libtool pkg-config \
                clang gcc g++ \
                clang-tidy gdb strace trace-cmd valgrind \
                curl git mc mercurial openssh-server pv p7zip-full tmux tree vim wget xclip
