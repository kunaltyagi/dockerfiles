FROM postgres:10

MAINTAINER Kunal Tyagi

RUN echo "deb http://deb.debian.org/debian stretch-backports main" >> /etc/apt/sources.list && \
    apt update && \
    apt install -y git && \
    cd /root && \
    pids="" && \
    for repo in rbock/sqlpp11 HowardHinnant/date Kitware/CMake matthijs/sqlpp11-connector-postgresql jtv/libpqxx pgRouting/pgrouting pgRouting/osm2pgrouting; do \
        git clone https://github.com/${repo}.git --depth=1 & \
        pids="$! ${pids}"; \
    done && \
    apt install -y build-essential clang-6.0 libcurl4-openssl-dev libboost-all-dev \
                   libcgal-dev postgresql-contrib postgis postgresql-10-postgis-scripts \
                   libtap-parser-sourcehandler-pgtap-perl postgresql-10-pgtap \
                   expat libexpat1-dev postgresql \
                   libpq-dev postgresql-server-dev-all python-dev && \
    for pid in ${pids}; do \
        wait ${pid}; \
    done && \
    ( \
        export PYTHONIOENCODING=utf-8 && \
        cd /root/libpqxx && \
        ./configure --disable-documentation && \
        make -j8 install & \
    ) && \
    cd /root/CMake && \
    ./bootstrap && make -j8 && make install && \
    mkdir /root/date/build && \
    cd /root/date/build && \
    cmake -DCMAKE_BUILD_TYPE=Release .. && \
    cmake --build . --target install -- -j8 && \
    mkdir /root/sqlpp11/build && \
    cd /root/sqlpp11/build && \
    cmake -DCMAKE_BUILD_TYPE=Release .. && \
    cmake --build . --target install -- -j8 && \
    mkdir /root/sqlpp11-connector-postgresql/build && \
    cd /root/sqlpp11-connector-postgresql/build && \
    cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_MODULE_PATH="/usr/local/lib/cmake/Sqlpp11" .. && \
    cmake --build . --target install -- -j8 && \
    mkdir /root/pgrouting/build && \
    cd /root/pgrouting/build && \
    cmake -DCMAKE_BUILD_TYPE=Release .. && \
    cmake --build . --target install -- -j8 && \
    mkdir /root/osm2pgrouting/build && \
    cd /root/osm2pgrouting/build && \
    cmake -DCMAKE_BUILD_TYPE=Release .. && \
    cmake --build . --target install -- -j8
